#ifndef TOUCHLISTENER_H
#define TOUCHLISTENER_H

#include <QObject>
#include <QtGui>
#include <QtQml>

#include <functional>
#include <chrono>

#include <QSettings>

#include "screenpwmcontroller.h"

class QTimer;
class ScreenPWMController;

class TouchListener : public QObject
{
    Q_OBJECT

public:
    using timeoutInterval_ms = std::chrono::milliseconds;
public:
    Q_INVOKABLE void lisenTo(QObject *a_object);
    bool eventFilter(QObject *a_object, QEvent *a_event) override;

    void onTimerTimeout();
    void touchEvent();

    static QObject *touchListenerInstance(QQmlEngine *a_engine, QJSEngine *a_jsEngine);
    Q_INVOKABLE int timeoutInterval() const;

signals:
    void onTimeoutIntervalChanged(int a_value);

public slots:
    void setPwmValue(int a_value);
    void setTimeoutInterval(int m_timeoutInterval);
    void writeSettings();

private:
    explicit TouchListener(QObject *parent = nullptr);

    void readSettings();
private:
    static TouchListener *m_listenerInstance;
    timeoutInterval_ms m_timeoutInterval{};
    QTimer *m_screenOffTimer{};
    ScreenPWMController m_screenController{};
    bool m_screenOff{ false };
    int m_savedPwmValue{};
    QSettings m_settings{};
};

#endif // TOUCHLISTENER_H
