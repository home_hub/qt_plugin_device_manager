#ifndef HARDWAREMANAGER_H
#define HARDWAREMANAGER_H

#include <QQuickItem>
#include <QTimer>
#include <QSettings>

#include "screenpwmcontroller.h"

class HardwareManager : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(HardwareManager)

public:
    HardwareManager();
    explicit HardwareManager(QQuickItem *parent);
    ~HardwareManager();

signals:
    void brightnessChanged(int a_screenBrightness);

public slots:
    int getBrightness() const;
    void setBrightness(int a_value);
    void writeSettings();

private:
    void readSettings();

private:
    ScreenPWMController m_screenController{};
    QSettings m_settings{};
};

#endif // HARDWAREMANAGER_H
