#ifndef RPIHARDWAREMANAGER_PLUGIN_H
#define RPIHARDWAREMANAGER_PLUGIN_H

#include <QQmlExtensionPlugin>

class RpiHardwareManagerPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // RPIHARDWAREMANAGER_PLUGIN_H
