#ifndef SCREENPWMCONTROLLER_H
#define SCREENPWMCONTROLLER_H

#include <QObject>

#include <wiringPi.h>

#define USE_HARDWARE_PWM

class ScreenPWMController : public QObject
{
    Q_OBJECT
public:
    ScreenPWMController();
    ScreenPWMController(int a_pwmPin, QObject *parent);

    void setPwmPin(int a_pin);

    int getPwmValue() const;
    void setPwmValue(int a_value);

signals:

public slots:

private:
    int m_pwmPin{};
    int m_pwmValue{};
};

#endif // SCREENPWMCONTROLLER_H
