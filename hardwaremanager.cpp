#include "hardwaremanager.h"

#include <functional>

#include <QSettings>
#include <QDebug>

#include "touchlistener.h"

HardwareManager::HardwareManager() : HardwareManager(nullptr)
{}

HardwareManager::HardwareManager(QQuickItem *parent):
    QQuickItem(parent),
    m_settings(QSettings(this))
{
    // By default, QQuickItem does not draw anything. If you subclass
    // QQuickItem to create a visual item, you will need to uncomment the
    // following line and re-implement updatePaintNode()

    // setFlag(ItemHasContents, true);
}

HardwareManager::~HardwareManager()
{
    readSettings();
}

int HardwareManager::getBrightness() const
{
    return m_screenController.getPwmValue();
}

void HardwareManager::setBrightness(int a_value)
{
    m_screenController.setPwmValue(a_value);

    emit brightnessChanged(a_value);
    writeSettings();
}

void HardwareManager::writeSettings()
{
    m_settings.beginGroup("Screen");
    m_settings.setValue("Brightness", m_screenController.getPwmValue());
    m_settings.endGroup();
    m_settings.sync();
}

void HardwareManager::readSettings()
{
    m_settings.beginGroup("Screen");
    int pwmValue = m_settings.value("Brightness", 1024).toInt();
    qDebug() << pwmValue;
    m_screenController.setPwmValue(pwmValue);
    m_settings.endGroup();
}
