﻿#include "touchlistener.h"

#include <QTimer>
#include <QDebug>

static constexpr int TOUCH_DEFAULT_TIMEOUT_MIN = 30;
static constexpr int PWM_DEFAULT_VALUE = 1024;

TouchListener *TouchListener::m_listenerInstance = nullptr;

TouchListener::TouchListener(QObject *parent) :
    QObject(parent),
    m_screenOffTimer(new QTimer(this)),
    m_savedPwmValue(PWM_DEFAULT_VALUE),
    m_settings(QSettings(this))
{
    readSettings();

    connect(m_screenOffTimer, &QTimer::timeout, this, &TouchListener::onTimerTimeout);
    m_screenOffTimer->setSingleShot(true);
    m_screenOffTimer->start(m_timeoutInterval);
}

int TouchListener::timeoutInterval() const
{
    return (m_timeoutInterval.count() / 1000);
}

void TouchListener::setTimeoutInterval(int a_timeoutInterval)
{
    m_timeoutInterval = std::chrono::milliseconds(a_timeoutInterval * 1000);
}

void TouchListener::lisenTo(QObject *a_object)
{
    if (!a_object) {
        return;
    }
    a_object->installEventFilter(this);
}

void TouchListener::onTimerTimeout()
{
    m_screenController.setPwmValue(0);
    m_screenOff = true;
}

void TouchListener::touchEvent()
{
    if(m_screenOff) {
        m_screenController.setPwmValue(m_savedPwmValue);
        m_screenOffTimer->start(m_timeoutInterval);
    } else {
        m_screenOffTimer->stop();
        m_screenOffTimer->start(m_timeoutInterval);
    }
}

void TouchListener::setPwmValue(int a_value)
{
    if (a_value == m_savedPwmValue) {
        return;
    }
    m_savedPwmValue = a_value;
}

QObject *TouchListener::touchListenerInstance([[maybe_unused]] QQmlEngine *a_engine, QJSEngine *a_jsEngine)
{
    if (!m_listenerInstance) {
        m_listenerInstance = new TouchListener(a_jsEngine);
    }
    return m_listenerInstance;
}

bool TouchListener::eventFilter([[maybe_unused]] QObject *a_object, QEvent *a_event)
{
    if (a_event->type() == QEvent::TouchBegin) {
        touchEvent();
        return false;
    }
    return false;
}

void TouchListener::writeSettings()
{
    m_settings.beginGroup("Screen");
    m_settings.setValue("Dimming_time_sec", (m_timeoutInterval.count() / 1000));
    m_settings.endGroup();
    m_settings.sync();
}

void TouchListener::readSettings()
{
    m_settings.beginGroup("Screen");
    int dimmingTime = m_settings.value("Dimming_time_sec", TOUCH_DEFAULT_TIMEOUT_MIN).toInt();
    setTimeoutInterval(dimmingTime);
    m_settings.endGroup();
}


