#include "rpihardwaremanager_plugin.h"

#include <qqml.h>

#include "hardwaremanager.h"
#include "touchlistener.h"

void RpiHardwareManagerPlugin::registerTypes(const char *uri)
{
    // @uri com.goro3.utils.hardwareManager
    qmlRegisterType<HardwareManager>(uri, 1, 0, "HardwareManager");
    qmlRegisterSingletonType<TouchListener>(uri, 1, 0, "TouchListener", TouchListener::touchListenerInstance);
}

