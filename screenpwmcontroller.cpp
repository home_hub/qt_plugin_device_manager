#include "screenpwmcontroller.h"

#include <QtDebug>

#include <wiringPi.h>

#ifdef USE_HARDWARE_PWM
    static constexpr int PWM_HARDWARE_PIN = 1;
#endif

static constexpr int PWM_DEFAULT_VALUE = 1024;

ScreenPWMController::ScreenPWMController() : ScreenPWMController(0, nullptr)
{}

ScreenPWMController::ScreenPWMController([[maybe_unused]] int a_pwmPin, QObject *parent)
    : QObject(parent)
#ifdef USE_HARDWARE_PWM
    ,m_pwmPin(PWM_HARDWARE_PIN)
#else
    , m_pwmPin(a_pwmPin)
#endif
    ,m_pwmValue(PWM_DEFAULT_VALUE)
{
    wiringPiSetup();
    pinMode(m_pwmPin, PWM_OUTPUT);
    pwmWrite(m_pwmPin, PWM_DEFAULT_VALUE);
}

void ScreenPWMController::setPwmValue(int a_pwmValue)
{
    if ((a_pwmValue < 0) || (a_pwmValue > 1024)) {
        qWarning() << "PWM Value is out of range (0 - 1024), use proper Value";
    }
    m_pwmValue = a_pwmValue;
    pwmWrite(m_pwmPin, a_pwmValue);
}

int ScreenPWMController::getPwmValue() const
{
    return m_pwmValue;
}
